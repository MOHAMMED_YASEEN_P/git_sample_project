mkdir hello/five/six/seven
mkdir hello/one/two/three/four
touch hello/five/six/c.txt
touch hello/five/six/seven/error.log
touch hello/one/a.txt hello/one/b.txt
touch hello/one/two/d.txt
touch hello/one/two/three/e.txt
touch hello/one/two/three/four/access.log

find -name "*.log" -type f -delete

echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others."> hello/one/a.txt

rm -r hello/five
